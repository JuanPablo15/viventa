<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AnuncioVendedor;
use App\Inmuebles;
class AnunciosVendedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $inmuebles=Inmuebles::all();
        return view('anunciosVendedor',['inmuebles'=> $inmuebles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $anunciovendedor = new AnuncioVendedor();
        
        $anunciovendedor->name = $request->name;
        $anunciovendedor->direccion = $request->direccion;
        $anunciovendedor->precio = $request->precio;
        $anunciovendedor->descripcion = $request->descripcion;

        if( $anunciovendedor->save())
        {
            return view('anunciosVendedor');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        dd($id);
        exit();
        AnuncioVendedor::destroy($id);
        return redirect('anunciosAdministrador');
    }
}
