<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mensajes;
class CMensajesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $mensaj=Mensajes::all();
        
        return view('Mensajes.mensajes',['mensajes'=>$mensaj]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        [ 
            'user_name' =>'required',
            'asunto' =>'required',
            'descripcion' =>'required',
            'email' =>'required',
          
       

        ]);
        $men= new Mensajes ();
        
        $men->user_name =$request->input('user_name');
        $men->asunto =$request->input('asunto');
        $men->descripcion =$request->input('descripcion');
        $men->email =$request->input('email');
        
       
        $men->save();
         return redirect ('/comprasViviendas')->with('success','Datos Guardados');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        Mensajes::destroy($id);
        return redirect ('/mensajes')->with('destroy','Datos Eliminados');
    }
}
