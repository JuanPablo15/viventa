<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inmuebles;
class InmueblesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    
    {
        $inmuebles=Inmuebles::all();
        
        return view('Inmuebles.inmuebles',['inmuebles'=> $inmuebles]);

        /*$inm = Inmuebles::orderby('id')->paginate(5);
        
        return view ('Inmuebles.inmuebles',compact('inm '));*/
       

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('Inmuebles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,
        [ 
           
            'tipo' =>'required',
            'descripcion' =>'required',
           

        ]);
        $inm= new Inmuebles ();
        
       
        $inm->tipo =$request->input('tipo');
        $inm->descripcion =$request->input('descripcion');
        

        $inm->save();
         return redirect ('/inmuebles')->with('success','Datos Guardados');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inmueble=Inmuebles::find($id);
       
        $inmueble ->tipo =$request->tipo;
        $inmueble ->descripcion =$request->descripcion;
       

        if ($inmueble ->save()){

            return redirect ('/inmuebles')->with('edit','Datos Actualizados');
        }
        else  
        {
            $inm = Inmuebles::orderby('id')->paginate(5);
        
            return view ('Inmuebles.inmuebles',compact('inm'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Inmuebles::destroy($id);
        return redirect ('/Inmuebles.inmuebles')->with('destroy','Datos Eliminados');
    }
}
