<?php

namespace App\Http\Controllers;
use App\users;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usa=users::all();
        
        return view('Usuarios.usuarios',['users'=> $usa]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        [ 
            'name' =>'required',
            'apellidos' =>'required',
            'email' =>'required',
            'tipo' =>'required',
            'password' =>'required',
       

        ]);
        $asu= new users ();
        
        $asu->name =$request->input('name');
        $asu->apellidos =$request->input('apellidos');
        $asu->email =$request->input('email');
        $asu->tipo =$request->input('tipo');
        $asu->password =bcrypt($request->input('password'));
       
        $asu->save();
         return redirect ('/usuarios')->with('success','Datos Guardados');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuarios=users::find($id);
        $usuarios->name =$request->name;
        $usuarios ->apellidos =$request->apellidos;
        $usuarios ->email =$request->email;
        $usuarios ->tipo =$request->tipo;
        $usuarios ->password =bcrypt($request->input('password'));
       

        if ($usuarios ->save()){

            return redirect ('/usuarios')->with('edit','Datos Actualizados');
        }
        else  
        {
            $usa=users::all();
        
        return view('Usuarios.usuarios',['users'=> $usa]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        users::destroy($id);
        return redirect ('/usuarios')->with('destroy','Datos Eliminados');
    }
}
