<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class SoloAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
  

    public function handle(Request $request, Closure $next)
       {
        $variable =auth::user()->tipo;
            if ($variable=="Administrador")
            {
                $v1='1';

           }
           else if($variable=="Vendedor")
           {
               $v1='2';
           }
           else if($variable=="Comprador")
           {
                $v1='3';
           }
           switch($v1){
              
            case ('1'):
                return $next($request);//si es administrador continua al HOME
            break;
            case('2'):
                return redirect('vendedor');// si es un usuario normal redirige a la ruta USER
            break;	
            case ('3'):
                return redirect('comprador');//si es administrador redirige al moderador
            break;
         }
       }
}
