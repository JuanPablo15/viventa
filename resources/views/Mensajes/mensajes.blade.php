@extends('template3')
@section('Container')
<div class="container">
<h4 class="grey-text center">MENSAJES RECIBIDOS POR USUARIOS</h4>
</div>
<div class="container">
<table class="table table-bordered">
<thead class="grey darken-2 white-text">
<tr>
<th class="center">ID</th>
<th class="center">NOMBRE DEL USUARIO</th>
<th class="center">ASUNTO</th>
<th class="center">DESCRIPCION</th>
<th class="center">EMAIL</th>
<th class="center">FECHA DE PUBLICACIÓN</th>
<th class="center">ELIMINAR</th>
</tr>
</thead>
<tbody class="white">
@foreach($mensajes as $mensaje)
<tr class="black-text center">
<td>{{$mensaje->id}}</td>
<td>{{$mensaje->user_name}}</td>
<td>{{$mensaje->asunto}}</td>
<td>{{$mensaje->descripcion}}</td>
<td>{{$mensaje->email}}</td>
<td>{{$mensaje->created_at}}</td>
<td><a href="#borrar"  class="waves-effect waves-light btn red modal-trigger">
<i class="material-icons center">delete</i></a></td>
</tr>
@endforeach()
</tbody>
</table>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div id="borrar" class="modal">
  <div class="modal-content">
  <h5 class="grey darken-3 white-text center card-panel">¿Estas seguro de eliminar este mensaje?</h5>  </div>
  <center>
  <h5>  Una vez borrado el mensaje, no podra deshacer los cambios</h5>
  </center>
  <br>
    <div class="modal-footer">
      <div class="row right-align">
      <form action="{{url('/mensajes/'.$mensaje->id)}}" method="POST" class="d-nline-block">
        {{method_field('DELETE')}}
          @csrf
          <center>
          <button class="btn waves-effect waves-light green" type="submit" name="action">Aceptar</button>
        <a class="btn red modal-close">Cancelar</a>
          </center>
      
      </form> 
      </div>   
   </div>
</div>




@endsection