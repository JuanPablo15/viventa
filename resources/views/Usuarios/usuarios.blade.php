@extends('template3')
@section('Container')
<div class="container">
<h4 class="grey-text center">USUARIOS REGISTRADOS <a class="btn-floating btn-Large waves-effect waves-light  blue-grey darken-3 pull-right align=right btn modal-trigger" href="#modal3" > <i class="material-icons">add_circle</i>Actualizar Usuarios </a></h4>
</div>
<div class="container"> 
<table class="table">
  <thead class="grey darken-2 white-text center">
    <tr>
      <th class="center">ID</th>
      <th class="center">NOMBRE</th>
      <th class="center">APELLIDOS</th>
      <th class="center">EMAIL</th>
      <th class="center">TIPO</th>
     <th class="center">CONTRASEÑA</th>
     <th class="center">ACCIONES</th>
     </tr>
  </thead>
  <tbody class="white">
  @foreach($users as $usadatos)
  <tr class="black-text center">
      <td class="center">{{$usadatos->id}}</td>
      <td class="center">{{$usadatos->name}}</td>
      <td class="center">{{$usadatos->apellidos}}</td>
      <td class="center">{{$usadatos->email}}</td>
      <td class="center">{{$usadatos->tipo}}</td> 
      <td class="center">{{$usadatos->password}}</td>
      <td class="center">
      
      
        <a href="#modal1"  class="waves-effect waves-light btn blue modal-trigger">
          <i class="material-icons center">update</i></a>
  
     
        <a href="#modal2"  class="waves-effect waves-light btn red modal-trigger">
          <i class="material-icons center">delete</i></a>
   </td>
     
      
@endforeach()
</tbody>
</table>
</div>
</div>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br >

<!-- Modales -->
<!-- Modal Structure -->

<div id="modal3" class="modal grey lighten-2">

<div class="container">
    <div class="row valign-wrapper">
        <div class="col l12 s6 m8 ">
            <div class="row white">
                <h5 class="grey darken-3 white-text center card-panel">Agregar  Usuarios</h5>
               
                <form action="{{route('usuarios.store')}}" method="POST">
    @csrf
      <div class="modal-body">
       
      <div class="form-group">
            <label for="">Nombre</label>
            <input type="text" name="name" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="">Apellidos</label>
            <input type="text" name="apellidos" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="">Email</label>
            <input class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                           required autocomplete="email" 
                           type="email" name="email" id="email" required>
                           @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
        </div>

        <div class="form-group">
            <label for="">Tipo</label>
            <input type="text" name="tipo" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="">Password</label>
            <input type="password" name="password" id="password" required autocomplete="current-password">
            <!-- <input type="password" name="password" class="form-control"> -->
        </div>



      </div>

      <div class="modal-footer">
        <button type="button" class="grey darken-3 btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="grey darken-3 btn btn-primary">Guardar</button>
      </div>
    </form> 
            </div>
        </div>
       </div>
</div>  
</div>

<div id="modal2" class="modal">
  <div class="modal-content">
  <h5 class="grey darken-3 white-text center card-panel">¿Estas seguro de eliminar este usuario?</h5>  </div>
  <center>
  <h5>  Una vez borrado el usuario, no podra deshacer los cambios</h5>
  </center>
  <br>
    <div class="modal-footer">
      <div class="row right-align">
        <form  id= "formDelete" action="{{route('usuarios.destroy',$usadatos->id)}}" data-action="{{route('usuarios.destroy',$usadatos->id)}}" method="POST" >
        @csrf  @method('DELETE')
       
          <center>
          <button class="btn waves-effect waves-light green" type="submit" name="action">Aceptar</button>
        <a class="btn red modal-close">Cancelar</a>
          </center>
      
      </form> 
      </div>   
   </div>
</div>
<div id="modal1" class="modal grey lighten-2">

<div class="container">
    <div class="row valign-wrapper">
        <div class="col l12 s6 m8 ">
            <div class="row white">
                <h5 class="grey darken-3 white-text center card-panel">Actualizar Usuarios</h5>
               
                <form action="{{route('usuarios.update', $usadatos->id)}}" method="POST">
                 @csrf @method('PUT')
                    <div class="row">
                        <div class="col l12 s12 input-field">
                        <i class="material-icons prefix">perm_identity</i>
                        <label for="">Nombre</label>
                        
                          <input type="text" name="name" value ="{{$usadatos->name}}" class="form-control">
                        </div>
                      </div>

                      <div class="row">
                        <div class="col l12 s12 input-field">
                        <i class="material-icons prefix">perm_identity</i>
                        <label for="">Apellidos</label>
                        
                          <input type="text" name="apellidos" value ="{{$usadatos->apellidos}}" class="form-control">
                        </div>
                      </div>

                      <div class="row">
                        <div class="col l12 s12 input-field">
                        <i class="material-icons prefix">perm_identity</i>
                        <label for="">Email</label>
                        
                          <input type="text" name="email" value ="{{$usadatos->email}}" class="form-control">
                        </div>
                      </div>

                      <div class="row">
                        <div class="col l12 s12 input-field">
                        <i class="material-icons prefix">perm_identity</i>
                        <label for="">tipo</label>
                        
                          <input type="text" name="tipo" value ="{{$usadatos->tipo}}" class="form-control">
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="col l12 s12 input-field">
                        <i class="material-icons prefix">perm_identity</i>
                        <label for="">Password</label>
                        
                          <input type="password" name="password" value ="{{$usadatos->password}}" class="form-control">
                        </div>
                      </div>


                        <div class="row right">
                            <div class="col l12 s12">
                                <button class="blue-grey darken-3 waves-effect waves-light btn"  type="close"><i class="material-icons right">send</i>
                                   Actualizar
                                   </button>
                                    <button  class="modal-close grey darken-3 waves-effect waves-light  btn"><i class="material-icons right">send</i>salir </button>
                                   
                            </div>
                         
                              
                            
                          </div> 

                          
                        </div>
                        
                    </div> 
                   </form>
            </div>
        </div>
       </div>
</div>  
</div>






@endsection