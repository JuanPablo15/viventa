@extends('template2')
@section('Container')
 
  <!-- busqueda-->

  <div class="container">
    <div class="row">
      <div class="col l12 m8 s12">
       <h5>   <p class="light- brown-text  text-darken-3 "> Habitacion Rural
          <input type="searchp">
          <a class="btn-floating btn-small waves-effect waves-light  brown"><i class="material-icons">search</i></a>
        </div>
    </p> </h5>
</h1>
    </div>
  </div>


  <div class="container">
    <div class="row">
         <!-- Card 1 -->
        <div class="col l6 s12 m8">
          <div class="card">
            <div class="card-image">
              <img src="img/habitacionr1.jpg" alt="" width="100" height="250">
              <span class="card-title">inmueble Jalos</span>
            </div>
            <div class="card-content">
              <p aling="justify">
                Fecha de publicación: 25/05/2020 <br>
                Dirección: Av. Valle escondido #30 <br>
                Precio: $4500.00 <br>
                Descripción: Cuenta con 2 recamaras con baño personal, cocina, comedor, patio,cochera y sala.
               <br><br>
              </p>
            </div>
            <div class="card-action">
                <p align="right">Contactar al vendedor
                    <a class="btn-floating btn-Large waves-effect waves-light   brown pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                  </p>
            </div>
          </div>
        </div>
        <!-- Card 2 -->
        <div class="col l6 s12 m8">
            <div class="card">
              <div class="card-image">
                <img src="img/habitacionr2.jpg" alt="" width="100" height="250">
                <span class="card-title">inmueble santa Maria</span>
              </div>
              <div class="card-content">
                <p aling="justify">
                  Fecha de publicación: 28/08/2021 <br>
                  Dirección: Av. Santa Maria #8 <br>
                 
                  Precio: $1500.00 <br>
                 
                  Descripción: Cuenta con 2 recamaras, 3 baños cocina, comedor, patio, alberca, cochera, sala,
                  lavandería, área de entretenimiento, etc. <br>
                </p>
              </div>
              <div class="card-action">
                  <p align="right">Contactar al vendedor
                      <a class="btn-floating btn-Large waves-effect waves-light   brown pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                    </p>
              </div>
            </div>
          </div>
      <!-- Card 3 -->
      <div class="col l6 s12 m8">
        <div class="card">
          <div class="card-image">
            <img src="img/habitacionr3.jpg" alt=""width="100" height="250">
            <span class="card-title">inmueble Michoacan</span>
          </div>
          <div class="card-content">
            <p aling="justify">
              Fecha de publicación: 06/04/2021 <br>
              Dirección: Av. 5 de mayo #18 <br>
              Precio: $1500.00 <br>
              Descripción: Cuenta con 1 recamara  con baño personal, cocina, comedor, patio,cochera y sala
             <br><br>
            </p>
          </div>
          <div class="card-action">
              <p align="right">Contactar al vendedor
                  <a class="btn-floating btn-Large waves-effect waves-light   brown pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                </p>
          </div>
        </div>
      </div>
      <!-- Card 4 -->
      <div class="col l6 s12 m8">
        <div class="card">
          <div class="card-image">
            <img src="img/habitacionr4.jpg" alt="" width="100" height="250">
          <span class="card-title">inmueble  Veracruz</span>
        </div>
        <div class="card-content">
          <p aling="justify">
            Fecha de publicación: 15/12/2020 <br>
            Dirección: Av. luis Jasso #9 <br>
            Precio: $2500.00 <br>
            Descripción: Cuenta con 3 recamaras  con baño personal, cocina, comedor, patio,cochera , gimanasio y sala
           <br><br>
          </p>
          </div>
          <div class="card-action">
              <p align="right">Contactar al vendedor
                  <a class="btn-floating btn-Large waves-effect waves-light   brown pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                </p>
          </div>
        </div>
      </div>
</div>
 
<!-- Modales -->
<!-- Modal Structure -->
<div id="modal1" class="modal grey lighten-2">

<div class="container">
    <div class="row valign-wrapper">
        <div class="col l12 s6 m8 ">
            <div class="row white">
                <h5 class="grey darken-3 white-text center card-panel">Contactar al vendedor</h5>
                <form action="{{route('mensajes.store')}}" method="POST">
    @csrf
                    <div class="row">
                        <div class="col l12 s12 input-field">
                          <i class="material-icons prefix">perm_identity</i>
                          <input class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                           required autocomplete="email" autofocus
                           type="email" name="email" id="email" required>
                          <label for="email">Email</label>
                          @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                      </div>
                      <div class="row">
                        <div class="col l12 s12 input-field">
                            <i class="material-icons prefix">perm_identity</i>
                            <input class="form-control @error('name') is-invalid @enderror" value=" {{ Auth::user()->name }}"
                               required autocomplete="name" autofocus
                               type="text" name="user_name" id="name" required>
                              <label for="name">Nombre</label>
                              @error('name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                             @enderror
                        </div>
                      </div>
                        <div class="row">
                            <div class="col l12  s12 input-field">
                                <i class="material-icons prefix">description</i>
                                <input class="form-control validate" value=""
                                   required autocomplete="descripcion" autofocus
                                   type="text" name="descripcion" id="descripcion" required>
                                  <label for="descripcion">Descripcion</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col l12  s12 input-field">
                                <i class="material-icons prefix">description</i>
                                <input class="form-control validate" value="Alquiler Habitacion rural"
                                   required autocomplete="asunto" autofocus
                                   type="text" name="asunto" id="asunto" required>
                                  <label for="asunto">Asunto</label>
                            </div>
                        </div>
                        <div class="row right">
                            <div class="col l12 s12">
                                <button class="  btn  grey darken-3 btn-dannger waves-effect waves-light btn" type="submit"><i class="material-icons right">send</i>
                                    ENVIAR
                                    </button>
                            </div>
                            
                          </div> 
                        </div>
                    </div> 
                   </form>
            </div>
        </div>
       </div>
</div>  
</div>
@endsection