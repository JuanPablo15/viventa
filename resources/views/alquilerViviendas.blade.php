@extends('template2')
@section('Container')
<!--  Busqueda -->
<div class="container">
      <div class="row">
        <div class="col l12 m8 s12">
         <h5>   <p class="light-blue-text  text-darken-3 "> Vviviendas Alquiler
            <input type="searchp">
            <a class="btn-floating btn-small waves-effect waves-light indigo "><i class="material-icons">search</i></a>
          </div>
      </p> </h5>
  </h1>
      </div>
    </div>

    
 <div class="container">
        <div class="row">
             <!-- Card 1 -->
            <div class="col l6 s12 m8">
              <div class="card">
                <div class="card-image">
                  <img src="img/viviendaA2.jpg" alt="" width="100" height="250">
                  <span class="card-title">Avenida Balboa </span>
                </div>
                <div class="card-content">
                  <p aling="justify">
                    Fecha de publicación: 26/03/2021 <br>
                    Dirección: 3 de mayo #92 <br>
                    Precio: $3660.00 <br>
                    Descripción: Cuenta con  2 recamaras, 1 baño cocina, comedor, patio y 
                    lavandería  <br><br>
                  </p>
                </div>
                <div class="card-action">
                    <p align="right">Contactar al vendedor
                        <a class="btn-floating btn-Large waves-effect waves-light  blue darken-4 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                      </p>
                </div>
              </div>
            </div>
            <!-- Card 2 -->
            <div class="col l6 s12 m8">
                <div class="card">
                  <div class="card-image">
                    <img src="img/viviendaA1.jpg" alt="" width="100" height="250">
        <span class="card-title "> Apartamentos en Punta Pacífica</span>
      </div>
      <div class="card-content">
        <p aling="justify">
          Fecha de publicación: 30/05/2020 <br>
          Dirección: Av. 5 de mayo #36 <br>
          Precio: $4.700.00 <br>
          Descripción: Cuenta con  3 r recamaras con baño personal, cocina, comedor, patio, alberca, cochera, sala,
          lavandería, etc.Incluye aire acondicionado. <br>
        </p>
                  </div>
                  <div class="card-action">
                      <p align="right">Contactar al vendedor
                          <a class="btn-floating btn-Large waves-effect waves-light  blue darken-4 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                        </p>
                  </div>
                </div>
              </div>
          <!-- Card 3 -->
          <div class="col l6 s12 m8">
            <div class="card">
              <div class="card-image">
                <img src="img/viviendaA4.jpg" alt="" width="100" height="250">
        <span class="card-title">inmueble Perez</span>
      </div>
      <div class="card-content">
        <p aling="justify">
          Fecha de publicación: 19/08/2020 <br>
          Dirección: Av. Miguel Hidalgo #5000 <br>
          Precio: $1000.00 <br>
          Descripción: Cuenta con  2 recamaras, 2 baños, cocina, comedor, patio, sala de entretenimiento, cochera, sala,
          lavandería, área de entretenimiento, etc. <br>
        </p>
              </div>
              <div class="card-action">
                  <p align="right">Contactar al vendedor
                      <a class="btn-floating btn-Large waves-effect waves-light  blue darken-4 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                    </p>
              </div>
            </div>
          </div>
          <!-- Card 4 -->
          <div class="col l6 s12 m8">
            <div class="card">
              <div class="card-image">
                <img src="img/viviendaA3.jpeg" alt=""width="100" height="250"> 
                <span class="card-title"> Inmueble Carlo's</span>
              
                </div>
              <div class="card-content">
            
                <p aling="justify">
                  Fecha de publicación: 8/04/2021 <br>
                  Dirección: Av.  Lopez Cotilla #60 <br>
                  Precio: $1500.00 <br>
                  Descripción: Cuenta con 1 recamara  con baño personal, cocina, comedor, patio,cochera y sala
                 <br><br>
                </p>
              </div>
              <div class="card-action">
                  <p align="right">Contactar al vendedor
                      <a class="btn-floating btn-Large waves-effect waves-light  blue darken-4 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                    </p>
              </div>
            </div>
          </div>
    </div>
     
    <!-- Modales -->
<!-- Modal Structure -->
<div id="modal1" class="modal grey lighten-2">

<div class="container">
    <div class="row valign-wrapper">
        <div class="col l12 s6 m8 ">
            <div class="row white">
                <h5 class="grey darken-3 white-text center card-panel">Contactar al vendedor</h5>
                <form action="{{route('mensajes.store')}}" method="POST">
    @csrf
                    <div class="row">
                        <div class="col l12 s12 input-field">
                          <i class="material-icons prefix">perm_identity</i>
                          <input class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                           required autocomplete="email" autofocus
                           type="email" name="email" id="email" required>
                          <label for="email">Email</label>
                          @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                      </div>
                      <div class="row">
                        <div class="col l12 s12 input-field">
                            <i class="material-icons prefix">perm_identity</i>
                            <input class="form-control @error('name') is-invalid @enderror" value=" {{ Auth::user()->name }}"
                               required autocomplete="name" autofocus
                               type="text" name="user_name" id="name" required>
                              <label for="name">Nombre</label>
                              @error('name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                             @enderror
                        </div>
                      </div>
                        <div class="row">
                            <div class="col l12  s12 input-field">
                                <i class="material-icons prefix">description</i>
                                <input class="form-control validate" value=""
                                   required autocomplete="descripcion" autofocus
                                   type="text" name="descripcion" id="descripcion" required>
                                  <label for="descripcion">Descripcion</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col l12  s12 input-field">
                                <i class="material-icons prefix">description</i>
                                <input class="form-control validate" value="Alquiler viviendas"
                                   required autocomplete="asunto" autofocus
                                   type="text" name="asunto" id="asunto" required>
                                  <label for="asunto">Asunto</label>
                            </div>
                        </div>
                        <div class="row right">
                            <div class="col l12 s12">
                                <button class="  btn  grey darken-3 btn-dannger waves-effect waves-light btn" type="submit"><i class="material-icons right">send</i>
                                    ENVIAR
                                    </button>
                            </div>
                            
                          </div> 
                        </div>
                    </div> 
                   </form>
            </div>
        </div>
       </div>
</div>  
</div>
@endsection