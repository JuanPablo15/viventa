@extends('template3')
@section('Container')
<div class="container">
<h4 class="grey-text center">ANUNCIOS REALIZADOS POR USUARIOS</h4>
</div>
<div class="container">
<table class="table table-bordered">
<thead class="grey darken-2 white-text">
<tr>
<th class="center">ID</th>
<th class="center">NOMBRE COMPLETO</th>
<th class="center">TIPO DE INMUEBLE</th>
<th class="center">DIRECCIÓN</th>
<th class="center">PRECIO</th>
<th class="center">DESCRIPCIÓN</th>
<th class="center">FECHA DE PUBLICACIÓN</th>
<th class="center">Eliminar</th>
</tr>
</thead>
<tbody class="white">
@foreach($anunciosAdministrador as $anuncio)
<tr class="black-text center">
<td>{{$anuncio->id}}</td>
<td>{{$anuncio->name}}</td>
<td>{{$anuncio->tipo}}</td>
<td>{{$anuncio->direccion}}</td>
<td>{{$anuncio->precio}}</td>
<td>{{$anuncio->descripcion}}</td>
<td>{{$anuncio->created_at}}</td>
<td><a href="#borrar"  class="waves-effect waves-light btn red modal-trigger">
<i class="material-icons center">delete</i></a></td>
</tr>
@endforeach
</tbody>
</table>
<br>
<br>
<br>
<br>
<br>
<br>
</div>
<div id="borrar" class="modal">
  <div class="modal-content">
  <h5 class="grey darken-3 white-text center card-panel">¿Estas seguro de eliminar este anuncio?</h5>  </div>
  <center>
  <h5>  Una vez borrado el anuncio, no podra deshacer los cambios</h5>
  </center>
  <br>
    <div class="modal-footer">
      <div class="row right-align">
      <form action="{{url('/anunciosVendedor/'.$anuncio->id)}}" method="POST" class="d-nline-block">
        {{method_field('DELETE')}}
          @csrf
          <center>
          <button class="btn waves-effect waves-light green" type="submit" name="action">Aceptar</button>
        <a class="btn red modal-close">Cancelar</a>
          </center>
      
      </form> 
      </div>   
   </div>
</div>
@endsection









