@extends('template4')

@section('Container')
<div class="container">
            <div class="row valign-wrapper">
              <div class="col s6 offset-s3 valign">
                  <div class="row white">
                      <h5 class="grey darken-3 white-text center card-panel">Registro de anuncios</h5>
                      <form method="POST" action="{{ route('anunciosVendedor.store') }}">
                          @csrf
                          <div class="row">
                              <div class="col l12 s12 input-field">
                                  <i class="material-icons prefix">perm_identity</i>
                                  <input class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}"
                                     required autocomplete="name" autofocus
                                     type="text" name="name" id="name" required>
                                    <label for="name">Nombre completo</label>
                              </div>
                          </div>
                          <div class="row">
                            <div class="col l12 s12 input-field">
                            <i class="material-icons prefix">home</i>
                            <select name="tipo" id="tipos">
                                 @foreach($inmuebles as $inmueble )
                                     <option>{{$inmueble->tipo}}  {{$inmueble->descripcion}}</option>
          
                                 @endforeach
                             </select>
                            </div>
                          </div>
                          <div class="row">
                              <div class="col l12 s12 input-field">
                                  <i class="material-icons prefix">directions</i>
                                  <input class="form-control" value="{{ old('direccion') }}"
                                     required 
                                     type="text" name="direccion" id="direccion" required>
                                    <label for="direccion">Direccion</label>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col l12 s12 input-field">
                                  <i class="material-icons prefix">attach_money</i>
                                  <input class="form-control" value="{{ old('precio') }}"
                                     required 
                                     type="text" name="precio" id="precio" required>
                                    <label for="precio">Precio</label>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col l12 s12 input-field">
                                  <i class="material-icons prefix">book</i>
                                  <input class="form-control" value="{{ old('descripcion') }}"
                                     required 
                                     type="text" name="descripcion" id="descripcion" required>
                                    <label for="descripcion">Descripcion</label>
                              </div>
                          </div>
                       
                          
                  
                            <div class="row center">
                              <button class=" green waves-effect waves-light btn" 
                              type="submit"><i class="material-icons right">send</i>
                              Anunciar
                              </button>
                              <button class=" red waves-effect waves-light btn" 
                              ><i class="material-icons right">cancel</i>
                              Cancelar
                              </button>
                            </div>
                      </form>
                  </div>
              </div>
            </div>
          </div> 
@endsection