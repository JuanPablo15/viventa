@extends('template1')

@section('content')
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, maximum-scale=1.0"
    />
    <title>Plantilla inicial Materialize</title>

    <!-- CSS  -->
    <link
      href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"
    />
    <link
      href="css/materialize.css"
      type="text/css"
      rel="stylesheet"
      media="screen,projection"
    />
    <link
      href="css/style.css"
      type="text/css"
      rel="stylesheet"
      media="screen,projection"
    />

</head>

<body class="grey lighten-2">

    <div class="container">
        <div class="row valign-wrapper">
            <div class="col s6 offset-s3 valign">
                <div class="row white">
                     <h5 class="grey darken-3 white-text center card-panel">Inicio de sesion</h5>
                     <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="row">
                            <div class="col l12 s12 input-field">
                              <i class="material-icons prefix">perm_identity</i>
                              <input class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                               required autocomplete="email" autofocus
                               type="email" name="email" id="email" required>
                              <label for="email">Email</label>
                              @error('email')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                            </div>
                          </div>
                          <div class="row">
                            <div class="col l12 s12 input-field">
                              <i class="material-icons prefix">lock</i>
                              <input class="form-control @error('password') is-invalid @enderror"
                               type="password" name="password" id="password" required autocomplete="current-password">
                              <label for="password">Contraseña</label>
                              @error('password')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                            </div>
                          </div>
                          <div class="row center">
                            <p>
                                <label>
                                  <input name ="remember" id="remember" type="checkbox" 
                                  class="filled-in" checked="checked" {{ old('remember') ? 'checked' : '' }} />
                                  <span>Recuerdame</span>
                                </label>
                              </p>
                          </div>
                          <div class="row center">
                            <button class=" grey darken-3 waves-effect waves-light btn" type="submit"><i class="material-icons right">send</i>
                            Iniciar
                            </button>
                            @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                      ¿Olvidaste tu contraseña?
                                    </a>
                                @endif
                          </div>
                       </form>
                </div>
            </div>
        </div>
      </div>
      
      <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script src="js/materialize.js"></script>
      <script src="js/init.js"></script>

</body>
</html>




@endsection
