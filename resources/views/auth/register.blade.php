@extends('template1')

@section('content')
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, maximum-scale=1.0"
    />
    <title>Plantilla inicial Materialize</title>

    <!-- CSS  -->
    <link
      href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"
    />
    <link
      href="css/materialize.css"
      type="text/css"
      rel="stylesheet"
      media="screen,projection"
    />
    <link
      href="css/style.css"
      type="text/css"
      rel="stylesheet"
      media="screen,projection"
    />
  </head>
  <body>
    <div class="container">
      <div class="row valign-wrapper">
        <div class="col s6 offset-s3 valign">
            <div class="row white">
                <h5 class="grey darken-3 white-text center card-panel">Registro de usuarios</h5>
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="row">
                        <div class="col l12 s12 input-field">
                            <i class="material-icons prefix">perm_identity</i>
                            <input class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}"
                               required autocomplete="name" autofocus
                               type="text" name="name" id="name" required>
                              <label for="name">Nombre</label>
                              @error('name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                             @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col l12 s12 input-field">
                            <i class="material-icons prefix">perm_identity</i>
                            <input class="form-control @error('apellidos') is-invalid @enderror" value="{{ old('apellidos') }}"
                               required autocomplete="apellidos" 
                               type="text" name="apellidos" id="apellidos" required>
                              <label for="apellidos">Apellidos</label>
                              @error('name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                             @enderror
                        </div>
                    </div>
                 
                    <div class="row">
                        <div class="col l12 s12 input-field">
                          <i class="material-icons prefix">perm_identity</i>
                          <input class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                           required autocomplete="email" 
                           type="email" name="email" id="email" required>
                          <label for="email">Email</label>
                          @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col l12 s12 input-field">
                          <i class="material-icons prefix">perm_identity</i>
                          <select name="tipo" id="tipos">
                          
                            <option>Comprador</option>
                            <option>Vendedor</option>
          
                            
                             </select>
            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col l12 s12 input-field">
                          <i class="material-icons prefix">lock</i>
                          <input class="form-control @error('password') is-invalid @enderror"
                           type="password" name="password" id="password" required autocomplete="current-password">
                          <label for="password">Contraseña</label>
                          @error('password')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                        </div>
                      </div>
                      <div class="row">
                        <div class="col l12 s12 input-field">
                            <i class="material-icons prefix">lock</i>
                            <input type="password" id="password" name="password_confirmation" required autocomplete="new-password">
                          <label for="password">Confirmar contraseña</label>
                        </div>
                      </div>
                      <div class="row center">
                        <button class=" grey darken-3 waves-effect waves-light btn" 
                        type="submit"><i class="material-icons right">send</i>
                        registrar
                        </button>
                      </div>
                </form>
            </div>
        </div>
      </div>
    </div>
     
    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="js/materialize.js"></script>
    <script src="js/init.js"></script>

  </body>
</html>

@endsection
