@extends('template2')

@section('Container')
<div class="container">
        <div class="row">
          <div class="col l12 m8 s12">
           <h5>   <p class="green-text text-darken-4  text-darken-3 "> Solares
              <input type="searchp">
              <a class="btn-floating btn-small waves-effect waves-light green darken-4 "><i class="material-icons">search</i></a>
            </div>
        </p> </h5>
          </h1>
        </div>
      </div>

<div class="container">
    <div class="row">
         <!-- Card 1 -->
        <div class="col l6 s12 m8">
          <div class="card">
            <div class="card-image">
                <img src="img/solares1.jpg" alt="" width="100" height="350">
        <span class="card-title">Solares 22473m²</span>
      </div>
      <div class="card-content">
        <p aling="justify">
          Fecha de publicación: 27/05/2021 <br>
          Dirección: San José Tzal <br>
          Precio: x <br>
          Descripción: Ubicado a 6.5 kilómetros del periférico de Mérida. <br>
          obre carretera Mérida - San Pedro Chimay.<br>
        </p>
            </div>
            <div class="card-action">
                <p align="right">Contactar al vendedor
                    <a class="btn-floating btn-Large waves-effect waves-light  green darken-4 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                  </p>
            </div>
          </div>
        </div>
        <!-- Card 2 -->
        <div class="col l6 s12 m8">
            <div class="card">
              <div class="card-image">
                <img src="img/solares2.jpg" alt="" width="100" height="350">
        <span class="card-title">Solares 23760m²</span>
      </div>
      <div class="card-content">
        <p aling="justify">
          Fecha de publicación: 21/03/2020 <br>
          Dirección: Pueblo Conkal, Conkal <br>
          Precio: x <br>
          Descripción:  lugar destacado por su paz y exclusividad;
          a tan solo minutos de plazas comerciales, hospitales,  <br>
        </p>
              </div>
              <div class="card-action">
                  <p align="right">Contactar al vendedor
                      <a class="btn-floating btn-Large waves-effect waves-light  green darken-4 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                    </p>
              </div>
            </div>
          </div>
      <!-- Card 3 -->
      <div class="col l6 s12 m8">
        <div class="card">
          <div class="card-image">
            <img src="img/solares3.jpg" alt=""width="100" height="350">
        <span class="card-title">Solares 2500m²</span>
      </div>
      <div class="card-content">
        <p aling="justify">
          Fecha de publicación: 7/04/2021 <br>
          Dirección: Cancún, Av Colegios <br>
          Precio: x <br>
          Descripción: Cuenta con salida a dos de las avenidas más importantes de la ciudad, 
          a tan solo 10 minutos del aeropuerto de Cancún. <br>
        </p>
          </div>
          <div class="card-action">
              <p align="right">Contactar al vendedor
                  <a class="btn-floating btn-Large waves-effect waves-light  green darken-4 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                </p>
          </div>
        </div>
      </div>
      <!-- Card 4 -->
      <div class="col l6 s12 m8">
        <div class="card">
          <div class="card-image">
            <img src="img/solares4.jpg" alt="" width="100" height="350">
        <span class="card-title">Solares 173m²</span>
      </div>
      <div class="card-content">
        <p aling="justify">
          Fecha de publicación: 10/12/2020 <br>
          Dirección: San Miguel de Allende, Guanajuato, Lagunita <br>
          Precio: x <br>
          Descripción: Cuentan Con Todos Los Servicios.<br>
          Listos para escriturar. <br>
        </p>
          </div>
          <div class="card-action">
              <p align="right">Contactar al vendedor
                  <a class="btn-floating btn-Large waves-effect waves-light  green darken-4 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                </p>
          </div>
        </div>
      </div>
</div>
 
<!-- Modales -->
<!-- Modal Structure -->
<div id="modal1" class="modal grey lighten-2">

<div class="container">
    <div class="row valign-wrapper">
        <div class="col l12 s6 m8 ">
            <div class="row white">
                <h5 class="grey darken-3 white-text center card-panel">Contactar al vendedor</h5>
                <form action="{{route('mensajes.store')}}" method="POST">
    @csrf
                    <div class="row">
                        <div class="col l12 s12 input-field">
                          <i class="material-icons prefix">perm_identity</i>
                          <input class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                           required autocomplete="email" autofocus
                           type="email" name="email" id="email" required>
                          <label for="email">Email</label>
                          @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                      </div>
                      <div class="row">
                        <div class="col l12 s12 input-field">
                            <i class="material-icons prefix">perm_identity</i>
                            <input class="form-control @error('name') is-invalid @enderror" value=" {{ Auth::user()->name }}"
                               required autocomplete="name" autofocus
                               type="text" name="user_name" id="name" required>
                              <label for="name">Nombre</label>
                              @error('name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                             @enderror
                        </div>
                      </div>
                        <div class="row">
                            <div class="col l12  s12 input-field">
                                <i class="material-icons prefix">description</i>
                                <input class="form-control validate" value=""
                                   required autocomplete="descripcion" autofocus
                                   type="text" name="descripcion" id="descripcion" required>
                                  <label for="descripcion">Descripcion</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col l12  s12 input-field">
                                <i class="material-icons prefix">description</i>
                                <input class="form-control validate" value="Compras solares "
                                   required autocomplete="asunto" autofocus
                                   type="text" name="asunto" id="asunto" required>
                                  <label for="asunto">Asunto</label>
                            </div>
                        </div>
                        <div class="row right">
                            <div class="col l12 s12">
                                <button class="btn  grey darken-3 btn-dannger waves-effect waves-light btn" type="submit"><i class="material-icons right">send</i>
                                    ENVIAR
                                    </button>
                            </div>
                            
                          </div> 
                        </div>
                    </div> 
                   </form>
            </div>
        </div>
       </div>
</div>  
</div>
@endsection