@extends('template2')
@section('Container')
<div class="container">
    <div class="row">
      <div class="col l12 m8 s12">
       <h5>   <p class="light-blue-text  text-darken-3 "> Terreno
          <input type="searchp">
          <a class="btn-floating btn-small waves-effect waves-light indigo "><i class="material-icons">search</i></a>
        </div>
    </p> </h5>
</h1>
    </div>
  </div>

 
  <div class="container">
    <div class="row">
         <!-- Card 1 -->
        <div class="col l6 s12 m8">
          <div class="card">
            <div class="card-image">
              <img src="img/terreno1.jpg" alt="" width="100" height="250">
          <span class="card-title">Terrenos Jose</span>
        </div>
        <div class="card-content">
          <p aling="justify">
            Fecha de publicación: 27/05/2020 <br>
            Dirección: Av. Conalep #52 <br>
            Precio: $106,000.00 <br>
            Descripción: Terreno de 1 hectarea en Parras Coah. a 130 mts. del camino a General Cepeda <br>
          </p>
            </div>
            <div class="card-action">
                <p align="right">Contactar al vendedor
                    <a class="btn-floating btn-Large waves-effect waves-light  blue darken-4 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                  </p>
            </div>
          </div>
        </div>
        <!-- Card 2 -->
        <div class="col l6 s12 m8">
            <div class="card">
              <div class="card-image">
                <img src="img/terreno2.jpg" alt="" width="100" height="250">
          <span class="card-title">Terrenos Angel</span>
        </div>
        <div class="card-content">
          <p aling="justify">
            Fecha de publicación: 28/05/2020 <br>
            Dirección: Av.Rita Perez #2 <br>
            Precio: $100,000.00 <br>
            Descripción: Terreno de 6 hectarea en AV.rita perez.  <br> <br>
          </p>
              </div>
              <div class="card-action">
                  <p align="right">Contactar al vendedor
                      <a class="btn-floating btn-Large waves-effect waves-light  blue darken-4 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                    </p>
              </div>
            </div>
          </div>
      <!-- Card 3 -->
      <div class="col l6 s12 m8">
        <div class="card">
          <div class="card-image">
            <img src="img/terreno3.jpg" alt=""width="100" height="250">
            <span class="card-title">Terrenos El Priul</span>
          </div>
          <div class="card-content">
            <p aling="justify">
              Fecha de publicación: 25/05/2020 <br>
              Dirección: Av.Rita Dominguez #9 <br>
              Precio: $153,000.00 <br>
              Descripción: Terreno de 9 hectarea  <br>
            </p>
          </div>
          <div class="card-action">
              <p align="right">Contactar al vendedor
                  <a class="btn-floating btn-Large waves-effect waves-light  blue darken-4 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                </p>
          </div>
        </div>
      </div>
      <!-- Card 4 -->
      <div class="col l6 s12 m8">
        <div class="card">
          <div class="card-image">
            <img src="img/terreno4.jpeg" alt="" width="100" height="250">
            <span class="card-title">Terrenos Guillermin</span>
          </div>
          <div class="card-content">
            <p aling="justify"></p>
              Fecha de publicación: 22/05/2020 <br>
              Dirección: Av. Nestle #20 <br>
              Precio: $500,000.00 <br>
              Descripción: Terreno de 10 hectarea <br>
            </p>
          </div>
          <div class="card-action">
              <p align="right">Contactar al vendedor
                  <a class="btn-floating btn-Large waves-effect waves-light  blue darken-4 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                </p>
          </div>
        </div>
      </div>
</div>
 
<!-- Modales -->
<!-- Modal Structure -->
<div id="modal1" class="modal grey lighten-2">

<div class="container">
    <div class="row valign-wrapper">
        <div class="col l12 s6 m8 ">
            <div class="row white">
                <h5 class="grey darken-3 white-text center card-panel">Contactar al vendedor</h5>
                <form action="{{route('mensajes.store')}}" method="POST">
    @csrf
                    <div class="row">
                        <div class="col l12 s12 input-field">
                          <i class="material-icons prefix">perm_identity</i>
                          <input class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                           required autocomplete="email" autofocus
                           type="email" name="email" id="email" required>
                          <label for="email">Email</label>
                          @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                      </div>
                      <div class="row">
                        <div class="col l12 s12 input-field">
                            <i class="material-icons prefix">perm_identity</i>
                            <input class="form-control @error('name') is-invalid @enderror" value=" {{ Auth::user()->name }}"
                               required autocomplete="name" autofocus
                               type="text" name="user_name" id="name" required>
                              <label for="name">Nombre</label>
                              @error('name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                             @enderror
                        </div>
                      </div>
                        <div class="row">
                            <div class="col l12  s12 input-field">
                                <i class="material-icons prefix">description</i>
                                <input class="form-control validate" value=""
                                   required autocomplete="descripcion" autofocus
                                   type="text" name="descripcion" id="descripcion" required>
                                  <label for="descripcion">Descripcion</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col l12  s12 input-field">
                                <i class="material-icons prefix">description</i>
                                <input class="form-control validate" value="Compras terrenos "
                                   required autocomplete="asunto" autofocus
                                   type="text" name="asunto" id="asunto" required>
                                  <label for="asunto">Asunto</label>
                            </div>
                        </div>
                        <div class="row right">
                            <div class="col l12 s12">
                                <button class="  btn  grey darken-3 btn-dannger waves-effect waves-light btn" type="submit"><i class="material-icons right">send</i>
                                    ENVIAR
                                    </button>
                            </div>
                            
                          </div> 
                        </div>
                    </div> 
                   </form>
            </div>
        </div>
       </div>
</div>  
</div>
@endsection