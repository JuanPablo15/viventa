@extends('template2')
@section('Container')
<div class="container">
        <div class="row">
          <div class="col l12 m8 s12">
           <h5>   <p class="blue-grey-text text-darken-3 ">Vivienda lujo
              <input type="searchp">
              <a class="btn-floating btn-small waves-effect waves-light blue-grey darken-3 "><i class="material-icons">search</i></a>
            </div>
        </p> </h5>
          </h1>
        </div>
      </div>

<div class="container">
    <div class="row">
         <!-- Card 1 -->
        <div class="col l6 s12 m8">
          <div class="card">
            <div class="card-image">
                <img src="img/viviendalujo1.jpg" alt="" width="100" height="350">
        <span class="card-title">Villa / Chalet de 2800 m2</span>
      </div>
      <div class="card-content">
        <p aling="justify">
            Fecha de publicación: 27/06/2020 <br>
            Dirección: zona residencial de Madrid: La Moraleja<br>
            Precio: x <br>
            Descripción: Cuenta con 11 recamaras con baño personal, Gimnasio con vistas al jardín,Spa con piscina de 60 m2,
            Salón cinema con unos 100 m2 ,Garaje, etc. <br>
          </p>
            </div>
            <div class="card-action">
                <p align="right">Contactar al vendedor
                    <a class="btn-floating btn-Large waves-effect waves-light  blue-grey darken-3 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                  </p>
            </div>
          </div>
        </div>
        <!-- Card 2 -->
        <div class="col l6 s12 m8">
            <div class="card">
              <div class="card-image">
                <img src="img/viviendalujo2.jpg" alt="" width="100" height="350">
                <span class="card-title ">Madrid España</span>
              </div>
              <div class="card-content">
                <p aling="justify">
                  Fecha de publicación: 21/08/2021 <br>
                  Dirección: zona de seguridad de Las Encinas <br>
                  Precio: x <br>
                  Descripción: 15.000m2 de parcela, bordeando la reserva natural, vestidor, salón privado, 
                  La suite principal tiene un gran vestidor y baño etc. <br>
                </p>
              </div>
              <div class="card-action">
                  <p align="right">Contactar al vendedor
                      <a class="btn-floating btn-Large waves-effect waves-light  blue-grey darken-3 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                    </p>
              </div>
            </div>
          </div>
      <!-- Card 3 -->
      <div class="col l6 s12 m8">
        <div class="card">
          <div class="card-image">
            <img src="img/viviendalujo3.jpg" alt="" width="100" height="350">
            <span class="card-title">Chalet de lujo de 1173 m2 </span>
          </div>
          <div class="card-content">
            <p aling="justify">
              Fecha de publicación: 7/09/2021 <br>
              Dirección:  zona de Valdemarin <br>
              Precio: x <br>
              Descripción: Cuenta comedor, salón TV, cocina, aseo de cortesía, despacho, cuarto de juegos, 
              habitación de invitados con su baño completo, etc. <br>
      
            </p>
          </div>
          <div class="card-action">
              <p align="right">Contactar al vendedor
                  <a class="btn-floating btn-Large waves-effect waves-light  blue-grey darken-3 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                </p>
          </div>
        </div>
      </div>
      <!-- Card 4 -->
      <div class="col l6 s12 m8">
        <div class="card">
          <div class="card-image">
            <img src="img/viviendalujo4..jpg" alt="" width="100" height="350">
            <span class="card-title">Casa adosada de lujo de 626 m2</span>
          </div>
          <div class="card-content">
            <p aling="justify">
              Fecha de publicación: 10/05/2021 <br>
              Dirección: LA FINCA Pº de los Lagos 2, Madrid <br>
              Precio: x <br>
              Descripción: Cuenta Amplia cocina con office y aseo de cortesía,salón-comedor de 95m2 con acceso al jardín y piscina privada  
              Sótano: Con luz natural y ventilación, etc. <br>
            </p>
          </div>
          <div class="card-action">
              <p align="right">Contactar al vendedor
                  <a class="btn-floating btn-Large waves-effect waves-light  blue-grey darken-3 pull-right align=right btn modal-trigger" href="#modal1" > <i class="material-icons">chat</i>Iniciar Chat </a>
                </p>
          </div>
        </div>
      </div>
</div>
 
<!-- Modales -->
<!-- Modal Structure -->
<div id="modal1" class="modal grey lighten-2">

<div class="container">
    <div class="row valign-wrapper">
        <div class="col l12 s6 m8 ">
            <div class="row white">
                <h5 class="grey darken-3 white-text center card-panel">Contactar al vendedor</h5>
                <form action="{{route('mensajes.store')}}" method="POST">
    @csrf
                    <div class="row">
                        <div class="col l12 s12 input-field">
                          <i class="material-icons prefix">perm_identity</i>
                          <input class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                           required autocomplete="email" autofocus
                           type="email" name="email" id="email" required>
                          <label for="email">Email</label>
                          @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                      </div>
                      <div class="row">
                        <div class="col l12 s12 input-field">
                            <i class="material-icons prefix">perm_identity</i>
                            <input class="form-control @error('name') is-invalid @enderror" value=" {{ Auth::user()->name }}"
                               required autocomplete="name" autofocus
                               type="text" name="user_name" id="name" required>
                              <label for="name">Nombre</label>
                              @error('name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                             @enderror
                        </div>
                      </div>
                        <div class="row">
                            <div class="col l12  s12 input-field">
                                <i class="material-icons prefix">description</i>
                                <input class="form-control validate" value=""
                                   required autocomplete="descripcion" autofocus
                                   type="text" name="descripcion" id="descripcion" required>
                                  <label for="descripcion">Descripcion</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col l12  s12 input-field">
                                <i class="material-icons prefix">description</i>
                                <input class="form-control validate" value="Compras viviendas lujo "
                                   required autocomplete="asunto" autofocus
                                   type="text" name="asunto" id="asunto" required>
                                  <label for="asunto">Asunto</label>
                            </div>
                        </div>
                        <div class="row right">
                            <div class="col l12 s12">
                                <button class="  btn  grey darken-3 btn-dannger waves-effect waves-light btn" type="submit"><i class="material-icons right">send</i>
                                    ENVIAR
                                    </button>
                            </div>
                            
                          </div> 
                        </div>
                    </div> 
                   </form>
            </div>
        </div>
       </div>
</div>  
</div>
@endsection