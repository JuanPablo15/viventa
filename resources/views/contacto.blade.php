@extends('template2')
@section('Container')
<div class="container">
    <div class="row valign-wrapper">
        <div class="col l12 s6 m8 ">
            <div class="row white">
                <h5 class="grey darken-3 white-text center card-panel">Envianos un correo</h5>
                <form action="{{route('mensajes.store')}}" method="POST">
    @csrf
                    <div class="row">
                        <div class="col l12 s12 input-field">
                          <i class="material-icons prefix">perm_identity</i>
                          <input class="form-control @error('email') is-invalid @enderror" value=""
                           required autocomplete="email" autofocus
                           type="email" name="email" id="email" required>
                          <label for="email">Email</label>
                          @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                      </div>
                      <div class="row">
                        <div class="col l12 s12 input-field">
                            <i class="material-icons prefix">perm_identity</i>
                            <input class="form-control @error('name') is-invalid @enderror" value="{{ Auth::user()->name }}"
                               required autocomplete="name" 
                               type="text" name="user_name" id="name" required>
                              <label for="name">Nombre</label>
                              @error('name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                             @enderror
                        </div>
                      </div>
                        <div class="row">
                            <div class="col l12  s12 input-field">
                                <i class="material-icons prefix">description</i>
                                <input class="form-control validate" 
                                   required autocomplete="descripcion" 
                                   type="text" name="descripcion" id="descripcion" required>
                                  <label for="descripcion">Descripcion</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col l12  s12 input-field">
                                <i class="material-icons prefix">description</i>
                                <input class="form-control validate" value=" "
                                   required autocomplete="asunto" autofocus
                                   type="text" name="asunto" id="asunto" required>
                                  <label for="asunto">Asunto</label>
                            </div>
                        </div>
                        <div class="row right">
                            <div class="col l12 s12">
                                <button class=" btn  grey darken-3 btn-dannger waves-effect waves-light btn" type="submit"><i class="material-icons right">send</i>
                                    ENVIAR
                                    </button>
                            </div>
                            
                          </div> 
                        </div>
                    </div> 
                   </form>
            </div>
        </div>
       </div>
</div>  
@endsection