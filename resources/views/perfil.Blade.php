@extends('template3')
@section('Container')
<div class="row">
      <div class="col l1 s12">
      </div>
      <div class="col l4 s12">
          <br><br>
        <div class="card z-depth-5">
          <div class="card-image grey darken-1">
            <img src="img/perfil.jpg" alt="">
            <span class="card-title">perfil</span>
          </div>
          <div class="card-action">
              <p align="right">
            <a href="#" class="blue-text">Cambiar foto de perfil</a></p>
          </div>
        </div>
      </div>
      <div class="col l6 s12">
        <div class="card z-depth-5">
          <div class="card-content">
          <form action="" method="POST">
          <label for="nombre">Nombre</label>
          <input type="text" name="nombre" id="nombre" value="{{ Auth::user()->name }}" required> 

          <label for="apellidos">Apellidos</label>
          <input type="text" name="apellidos" id="apellidos" value="{{ Auth::user()->apellidos }}" required>
         
          <label for="tipo">Tipo de usuario</label>
          <input type="text" name="tipo" id="tipo" value="{{ Auth::user()->tipo }}" required> 
         
          <label for="Contrasenia">Contraseña</label>
          <input type="password" name="password" id="password" value="{{ Auth::user()->password }}"required>
          
          <label for="email">e-mail</label>
          <input type="email" name="email" id="email" value="{{ Auth::user()->email }}"required>
          </div>
          <div class="card-action">
          <p align="right">
            <a href="#" class="blue-text">Actualizar mis datos</a> </p>
          </div>
          </form>
        </div>
      </div>
      <div class="col l1 s12">
      </div>
</div>
@endsection