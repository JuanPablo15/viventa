<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

      <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- CSS  -->
    <link
      href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"
    />
    <link
      href="css/materialize.css"
      type="text/css"
      rel="stylesheet"
      media="screen,projection"
    />
    <link
      href="css/style.css"
      type="text/css"
      rel="stylesheet"
      media="screen,projection"
    />

</head>

<body class="grey lighten-2">

<div class="navbar-fixed">
  
    <nav class="grey darken-3">
        <div class="container">
            <a href="" class="brand-logo hide-on-small-only white-text">Viventa</a>
            <a class="brand-logo show-on-small hide-on-large-only hide-on-med-only white-text text-darken-2"
               href="" style="font-size: 5vw;">
                Stanley</a>
            <ul class="right hide-on-med-and-down">
                <li><a href="{{ route('login') }}">Iniciar sesion</a></li>
                @if (Route::has('register'))
                <li><a href="{{ route('register') }}">Registrar</a></li>
                @endif
            </ul>
        </div>
    </nav>
 
</div>

@yield('content')

<footer class="page-footer grey darken-3">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5>Encuentra aqui tu proximo hogar</h5>
                <p aling="justify">
Empresa dedicada a la gestion de propiedades inmobiliarias dentro del panorama nacional
                </p>
            </div>
            <div class="col l2 s12"></div>
            <div class="col l4 s12">
                <h5>Contactanos</h5>
                <ul>
                    <li><i class="material-icons">facebook</i>viventaOficial <br>
                        <i class="material-icons">email</i>viventaOficial@gmail.com <br>
                        <i class="material-icons">phone</i>4741285003
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright col l12 grey darken-1">
        <div class="center container">
            <i class="material-icons">copyright</i>
            Todos los derechos reservados 2020.
        </div>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>
</body>
</html>

