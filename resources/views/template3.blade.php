<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

      <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- CSS  -->
    <link
      href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"
    />
    <link
      href="css/materialize.css"
      type="text/css"
      rel="stylesheet"
      media="screen,projection"
    />
    <link
      href="css/style.css"
      type="text/css"
      rel="stylesheet"
      media="screen,projection"
    />

</head>

<body class="grey lighten-2">
<div class="navbar-fixed">
      <nav class="grey darken-3">
          <div class="container">
              <a href="home" class="brand-logo hide-on-small-only white-text">Viventa Admin</a>
              <a class="brand-logo show-on-small hide-on-large-only hide-on-med-only white-text text-darken-2"
                 href="" style="font-size: 5vw;">
                  Viventa</a>
                    
                
                  <div class="nav-wrapper">
                  <ul class="right hide-on-med-and-down">
                  <li><a class="nav-link" href="home">Inicio </a></li>   
                 
                  <li><a class="nav-link" href="usuarios">Usuarios</a></li> 
                  <li><a class="nav-link" href="anunciosAdministrador">Anuncios</a></li>
                  <li><a class="nav-link" href="inmuebles">Inmuebles</a></li>  
                  <li><a class="nav-link" href="mensajes">Mensajes</a></li> 
                  
                 
                  <li> <a id="navbarDropdown" class="nav-link dropdown-toggle" href="perfil" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>
                  </li>
                  <li><a href="{{ route('logout') }} "
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    Cerrar sesion</a>  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form></li>
                  </div>
              </ul>            
          </div>
      </nav>
  </div>
  @yield('Container')

<footer class="page-footer grey darken-3">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5>Encuentra aqui tu proximo hogar</h5>
                <p aling="justify">
          Empresa dedicada a la gestion de propiedades inmobiliarias dentro del panorama nacional
                </p>
            </div>
            <div class="col l2 s12"></div>
            <div class="col l4 s12">
                <h5>Contactanos</h5>
                <ul>
                    <li><i class="material-icons">facebook</i>viventaOficial <br>
                        <i class="material-icons">email</i>viventaOficial@gmail.com <br>
                        <i class="material-icons">phone</i>4741285003
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright col l12 grey darken-1">
        <div class="center container">
            <i class="material-icons">copyright</i>
            Todos los derechos reservados 2020.
        </div>
    </div>
</footer>


<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>
<script>
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
    $('.sidenav').sidenav();
  });

  $(".dropdown-trigger").dropdown();

  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
    $('.modal').modal();
  });
</script>

</body>
</html>

