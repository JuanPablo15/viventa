@extends('template4')
@section('Container')
<div class="container">
  <div class="row">
    <div class="col l12 m8 s12">
    <h1 class=" center light-blue-text  text-darken-3">Viventa</h1>
    </div>
  </div>
  <div>
    <p class="center light-blue-text  text-darken-3">Encuentra aqui tu hogar </p>
  </div>
  <div class="video-container">
    <iframe width="560" height="315"
      src="https://www.youtube.com/embed/dmh8JExsNt4" title="YouTube video player" 
      frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
      allowfullscreen>
    </iframe>
  </div>
</div>
<br>
@endsection