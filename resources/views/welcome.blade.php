@extends('template1')
@section('content')
<div class="row card-panel black darken-3 white-text">
        <div class="container">
            <div class="col l8 m6 s12">
                <h5>Encuentra aqui tu proximo hogar</h5>
                <span class="blue-text text-darken-2"> Empresa</span> 
                dedicada a la gestión de propiedades inmobiliarias <BR> dentro del
                <span class="blue-grey-text text-lighten-3 "> panorama nacional
                </span> 
            </div>
            <div class="col l4 m4 s12">
                <img src="img/imagen1.jpeg" alt="" class="responsive-img" width="300" height="300">

            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col l12 s12">
                <img src="img/imagen2.jpeg" alt="" class="responsive-img" width="1200" height="1200">
    
            </div>
        </div>
    </div>
@endsection