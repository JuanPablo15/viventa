<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ModeradorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('comprasViviendas','ComprasViviendaController@index')->name('comprasViviendas');
Route::get('comprasViviendasLujo','ComprasViviendaLujoController@index')->name('comprasViviendasLujo');
Route::get('comprasSolares','ComprasSolaresController@index')->name('comprasSolares');
Route::get('comprasParcelas','ComprasParcelasController@index')->name('comprasParcelas');
Route::get('comprasTerrenos','ComprasTerrenosController@index')->name('comprasTerrenos');

Route::get('alquilerViviendas','AlquilerViviendasController@index')->name('alquilerViviendas');
Route::get('alquilerHabitacionesLujo','AlquilerHabitacionesLujoController@index')->name('alquilerHabitacionesLujo');
Route::get('alquilerHabitacionesPlaya','AlquilerHabitacionesPlayaController@index')->name('alquilerHabitacionesPlaya');
Route::get('alquilerHabitacionesRurales','AlquilerHabitacionesRuralesController@index')->name('alquilerHabitacionesRurales');

Route::get('contacto','ContactoController@index')->name('contacto');

Route::get('perfil','PerfilController@index')->name('perfil');
Route::get('perfilVendedor','PerfilVendedorController@index')->name('perfilVendedor');
Route::get('perfilComprador','PerfilCompradorController@index')->name('perfilComprador');


Route::get('mensajes','CMensajesController@index')->name('mensajes');
route::resource('mensajes', 'CMensajesController');



Route::get('inmuebles','InmueblesController@index')->name('inmuebles');
route::resource('inmuebles','InmueblesController');


Route::get('usuarios','usuariosController@index')->name('usuarios');
route::resource('usuarios','UsuariosController');
route::resource ('/usuarios','UsuariosController');

Route::get('anunciosVendedor','AnunciosVendedorController@index')->name('anunciosVendedor');
Route::resource('anunciosVendedor','AnunciosVendedorController');


Route::get('anunciosAdministrador','AnunciosAdministradorController@index')->name('anunciosAdministrador');
Route::resource('anunciosAdministrador','AnunciosAdministradorController');

Route::resource('/vendedor', VendedorController::class);
Route::resource('/comprador', CompradorController::class);
